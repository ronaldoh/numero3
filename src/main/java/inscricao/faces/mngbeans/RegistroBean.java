/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inscricao.faces.mngbeans;

import inscricao.entity.Candidato;
import java.util.ArrayList;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import utfpr.faces.support.PageBean;

/**
 *
 * @author RAH
 */
@Named(value = "registroBean")
@ApplicationScoped
public class RegistroBean extends PageBean {

    private ArrayList<Candidato> CandidatoList = new ArrayList<>();
    
    /**
     * Creates a new instance of RegistroBean
     */
    public RegistroBean() {
    }

    public ArrayList<Candidato> getCandidatoList() {
        return CandidatoList;
    }

    public void setCandidatoList(ArrayList<Candidato> CandidatoList) {
        this.CandidatoList = CandidatoList;
    }
    
    public void addCandidato(Candidato c){
        CandidatoList.add(c);
    }
}
